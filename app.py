# from cgescripts.run_cra import run_cra_script
from cgescripts.copy_modules import copy_modules_script
from cgescripts.config_project.main import config_project_script

# run_cra_script() Only run cra when necessary (to update it's dependencies)
copy_modules_script()
config_project_script()
