import os
import shutil
from cgescripts.utils.utils import get_path

path_prefix = get_path()
cra_path = path_prefix.joinpath("front-end/cg-related-config/cra")
cra_generated_path = cra_path.joinpath("cra-generated")
cmd_createreactapp = f"cd {cra_path} && npm i && npm run create"


def run_cmd():
    os.system(cmd_createreactapp)
    shutil.rmtree(cra_generated_path.joinpath("node_modules"))


def run_cra_script():
    try:
        shutil.rmtree(cra_generated_path)
        run_cmd()
    except Exception:
        run_cmd()
