import json
from cgescripts.utils.utils import get_path


path_prefix = get_path()
with open(path_prefix.joinpath("manifest.json")) as json_file:
    manifest = json.load(json_file)


modules_imports = """"""
for module in manifest:
    modules_imports += f"""
import {module["name"]} from \'{module["relPath"]}\'"""


nav_links = """"""
for module in manifest:
    nav_links += f"""
  {{
    name: '{module["name"]}',
    path: '/{module["name"].lower()}',
    icon: getIcon('{module["styles"]["navIcon"]}')
  }},"""


routes = """"""
for module in manifest:
    routes += f"""
        <Route path='/{module["name"].lower()}' component={{{module["name"]}}} />
"""
routes += f"""        <Redirect to='/{manifest[0]["name"].lower()}' />"""


app_file = f"""
import React from 'react'
import {{ Switch, Route, Redirect }} from 'react-router-dom'

import SidebarLayout from 'example-template/src/components/CustomSidebar/CustomSidebar'
import getIcon from './utils/getIcon'
{modules_imports}

const links = [{nav_links}
]

const App = () => (
  <div>
    <SidebarLayout links={{links}}>
      <Switch>{routes}
      </Switch>
    </SidebarLayout>
  </div>
)

export default App
"""


def get_appjs():
    return app_file
