from cgescripts.config_project.build_appjs import get_appjs
from cgescripts.utils.utils import modify_json, write_file


def config_project_script():
    modify_json(
        source="front-end/development/package.json",
        destination="front-end/generated-app/package.json",
        changes={
            "scripts": {"start": "craco start", "build": "craco build"},
            "devDependencies": {},
        },
    )

    write_file(
        destination="front-end/generated-app/src/App.js", content=get_appjs(),
    )
