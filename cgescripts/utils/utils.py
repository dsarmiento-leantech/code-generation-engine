import shutil
import json
from pathlib import Path


def get_path():
    path_prefix = Path.cwd()
    return path_prefix


path_prefix = get_path()


def create_dir(dirName):
    dir_to_manage = path_prefix.joinpath(dirName)
    try:
        Path(dir_to_manage).mkdir()
    except FileExistsError:
        shutil.rmtree(dir_to_manage)
        Path(dir_to_manage).mkdir()


def copy_dir(source, destination, folder_name=None):
    try:
        shutil.copytree(
            path_prefix.joinpath(source),
            path_prefix.joinpath(f"{destination}/{folder_name if folder_name else ''}"),
            dirs_exist_ok=True,
        )
    except Exception:
        print("Error, couldn't copy directory")


def copy_file(source, destination, folder_name=None):
    shutil.copy(
        path_prefix.joinpath(source),
        path_prefix.joinpath(f"{destination}/{folder_name if folder_name else ''}"),
    )


def write_file(destination, content):
    f = open(path_prefix.joinpath(destination), "w")
    f.write(content)
    f.close()


def modify_json(source, destination, changes=None):
    with open(path_prefix.joinpath(source)) as json_file:
        json_obj = json.load(json_file)

    for prop in list(changes.keys()):
        json_obj[prop] = changes[prop]

    write_file(path_prefix.joinpath(destination), json.dumps(json_obj))
