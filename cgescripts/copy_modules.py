import json
from cgescripts.utils.utils import create_dir, copy_dir, copy_file, get_path

path_prefix = get_path()
path_modules_destination = "front-end/generated-app/src/app-modules"

configs = [
    {
        "used_as": "dir",
        "source": "front-end/cg-related-config/cra/cra-generated",
        "destination": "front-end/generated-app",
        "folder_name": "",
    },
    {
        "used_as": "dir",
        "source": "front-end/development/src/example-template",
        "destination": "front-end/generated-app/src",
        "folder_name": "example-template",
    },
    {
        "used_as": "dir",
        "source": "front-end/development/src/utils",
        "destination": "front-end/generated-app/src",
        "folder_name": "utils",
    },
    {
        "used_as": "file",
        "source": "front-end/development/craco.config.js",
        "destination": "front-end/generated-app/craco.config.js",
        "folder_name": "",
    },
    {
        "used_as": "file",
        "source": "front-end/development/path-aliases.js",
        "destination": "front-end/generated-app/path-aliases.js",
        "folder_name": "",
    },
    {
        "used_as": "file",
        "source": "front-end/development/src/index.js",
        "destination": "front-end/generated-app/src/index.js",
        "folder_name": "",
    },
]

with open(path_prefix.joinpath("manifest.json")) as json_file:
    manifest = json.load(json_file)


def copy_modules_script():
    create_dir(dirName="front-end/generated-app")

    for config in configs:
        if config["used_as"] == "file":
            copy_file(source=config["source"], destination=config["destination"])
        else:
            copy_dir(
                source=config["source"],
                destination=config["destination"],
                folder_name=config["folder_name"],
            )

    for module in manifest:
        copy_dir(
            source=module["absPath"],
            destination=path_modules_destination,
            folder_name=module["name"],
        )
