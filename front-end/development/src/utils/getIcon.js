import InboxIcon from '@material-ui/icons/MoveToInbox'
import Store from '@material-ui/icons/Store'
import LocalShippingIcon from '@material-ui/icons/LocalShipping'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

const getIcon = (icon) => {
  switch (icon) {
    case 'InboxIcon':
      return InboxIcon
    case 'Store':
      return Store
    case 'LocalShippingIcon':
      return LocalShippingIcon
    case 'CheckCircleIcon':
      return CheckCircleIcon
    default:
      return InboxIcon
  }
}

export default getIcon
