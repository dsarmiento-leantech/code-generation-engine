import React from 'react'
import Cards from './DashboardCards'
import getIcon from '../../utils/getIcon'

const data = [
  {
    color: 'warning',
    icon: getIcon('Store'),
    name: 'Trucks in warehouse',
    value: '49',
    updated: 'Last hour'
  },
  {
    color: 'primary',
    icon: getIcon('LocalShippingIcon'),
    name: 'Trucks on the road',
    value: '32',
    updated: 'Last 2 hours'
  },
  {
    color: 'success',
    icon: getIcon('CheckCircleIcon'),
    name: 'Shipments delivered',
    value: '105',
    updated: 'Last 6 hours'
  }
]

const Dashboard = () => (
  <div>
    <Cards cardsData={data} />
  </div>
)

export default Dashboard
