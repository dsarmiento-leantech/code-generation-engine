import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import DateRange from '@material-ui/icons/DateRange'
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardIcon from 'components/Card/CardIcon.js'
import CardFooter from 'components/Card/CardFooter.js'
import styles from 'assets/jss/material-dashboard-react/views/dashboardStyle.js'

const useStyles = makeStyles(styles)

const DashboardCards = ({ cardsData }) => {
  const classes = useStyles()

  return (
    <GridContainer>
      {cardsData.map(({ color, icon: Icon, name, value, updated }, index) => (
        <GridItem xs={12} sm={6} md={4} key={index}>
          <Card>
            <CardHeader stats icon>
              <CardIcon color={color}>
                <Icon />
              </CardIcon>
              <p className={classes.cardCategory}>{name}</p>
              <h3 className={classes.cardTitle}>
                {value}
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <DateRange />
                {updated}
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      ))}
    </GridContainer>
  )
}

export default DashboardCards

DashboardCards.propTypes = {
  cardsData: PropTypes.array
}
