import React, { useState } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import SidebarLayout from 'example-template/src/components/CustomSidebar/CustomSidebar'
import Example from 'app-modules/Example/Example'
import Dashboard from 'app-modules/Dashboard/Dashboard'
import getIcon from './utils/getIcon'

const links = [
  {
    name: 'Example',
    path: '/example',
    icon: getIcon('InboxIcon')
  },
  {
    name: 'Dashboard',
    path: '/dashboard',
    icon: getIcon('InboxIcon')
  }
]

const App = () => {
  const [activeRoute, setActiveRoute] = useState('example')

  const onLinkClicked = (linkName) => setActiveRoute(linkName)

  return (
    <div>
      <SidebarLayout navbarTitle={activeRoute} links={links} onLinkClicked={onLinkClicked}>
        <Switch>
          <Route path='/example' component={Example} />
          <Route path='/dashboard' component={Dashboard} />
          <Redirect to='/example' />
        </Switch>
      </SidebarLayout>
    </div>
  )
}

export default App
