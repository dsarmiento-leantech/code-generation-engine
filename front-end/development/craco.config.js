const path = require('path')
const aliases = require('./path-aliases')

module.exports = {
  webpack: {
    alias: aliases(path.join(__dirname, '/src/'))
  }
}
