const path = require('path')

const aliases = (prefix) => {
  const dirPath = prefix || __dirname

  return {
    'app-modules': path.resolve(dirPath, 'app-modules/'),
    'example-template': path.resolve(dirPath, 'example-template/'),
    assets: path.resolve(dirPath, 'example-template/src/assets/'),
    components: path.resolve(dirPath, 'example-template/src/components/'),
    layouts: path.resolve(dirPath, 'example-template/src/layouts/'),
    variables: path.resolve(dirPath, 'example-template/src/variables/'),
    views: path.resolve(dirPath, 'example-template/src/views/')
  }
}

module.exports = aliases
